This git repo require git-lfs.

To install:

    git lfs install

Or:

    sudo apt install git-lfs

To clone this repo and fetch the binaries:

    git clone https://0xacab.org/riseup/public-riseup-images.git
    git lfs fetch origin master

Unless otherwise noted, all images are copyright Riseup Networks 2018
